/*
 * SMART360 Preprocessing
 * Copyright (C) 2023  Quentin Guimard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * This file contains code under the following license:
 *
 *
 * Copyright 2017 Université Nice Sophia Antipolis (member of Université Côte d'Azur), CNRS
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package preprocessing;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Worklist {
    private final List<Version> versions;
    private final List<Tiling> tilings;
    private final List<Integer> segments;
    private String videoPath;
    private int videoWidth;
    private int videoHeight;
    private int videoFPS;
    private String[][] tilingVersions;
    private String output;
    private String outputName;

    public Worklist() {
        this.versions = new ArrayList<>();
        this.tilings = new ArrayList<>();
        this.segments = new ArrayList<>();
    }

    public void setVideoPath(String videoPath) {
        this.videoPath = videoPath;
    }


    public void addVersion(Version version) {
        versions.add(version);
    }

    public void addTiling(Tiling tiling) {
        tilings.add(tiling);
    }

    public void addSegment(int segment) {
        segments.add(segment);
    }

    public void setOutput(String output) {
        this.output = output;
    }


    /**
     * Check if the video file provided in input exists
     *
     * @throws FileNotFoundException
     */
    public void videoExists() throws FileNotFoundException {
        File f = new File(videoPath);
        if (!f.exists() || f.isDirectory())
            throw new FileNotFoundException("The video file provided doesn't exist!");
    }

    /**
     * Checks if the directory provided as output exists: if so recursively delete its contents and re-create it.
     * This method also creates the full output file tree.
     *
     * @throws SecurityException
     */
    public void outputDirectory() throws SecurityException, IOException {
        File root = new File(output);
        outputName = root.getName();
        if (root.exists()) {
            Files.walk(root.toPath())
                    .sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .forEach(File::delete);
        }
        Files.createDirectories(root.toPath());

        for (Tiling tiling : tilings) {
            String outputTiling = String.format("%s/%dx%d", output, tiling.gethTiles(), tiling.getvTiles());
            File outputTilingFile = new File(outputTiling);
            if (outputTilingFile.exists()) {
                Files.walk(outputTilingFile.toPath())
                        .sorted(Comparator.reverseOrder())
                        .map(Path::toFile)
                        .forEach(File::delete);
            }
            Files.createDirectories(outputTilingFile.toPath());

            for (int segment : segments) {
                String outputSegment = String.format("%s/%d", outputTiling, segment);
                File outputSegmentFile = new File(outputSegment);
                if (outputSegmentFile.exists()) {
                    Files.walk(outputSegmentFile.toPath())
                            .sorted(Comparator.reverseOrder())
                            .map(Path::toFile)
                            .forEach(File::delete);
                }
                Files.createDirectories(outputSegmentFile.toPath());
            }
        }
    }

    /**
     * This method retrieves the video width and the video height and these parameters are set
     * as internal variables.
     *
     * @throws IOException
     * @throws InterruptedException
     */
    public void videoDimensions() throws IOException, InterruptedException {
        //TODO: check the buffer exception
        Runtime runTime = Runtime.getRuntime();

        //Preparing the commands to be launched
        //Width
        String widthCommand = "ffprobe -v error -of flat=s=_ -select_streams v:0 -show_entries stream=width " + videoPath;
        //Height
        String heightCommand = "ffprobe -v error -of flat=s=_ -select_streams v:0 -show_entries stream=height " + videoPath;
        //FPS
        String fpsCommand = "ffprobe -v error -of flat=s=_ -select_streams v:0 -show_entries stream=avg_frame_rate " + videoPath;

        //Width
        Process widthProcess = runTime.exec(widthCommand);
        ProcessGetDimensions widthOutput = new ProcessGetDimensions(widthProcess.getInputStream());
        widthOutput.start();
        widthProcess.waitFor();
        videoWidth = Integer.parseInt(widthOutput.returnValue());
        widthProcess.destroy();

        //Height
        Process heightProcess = runTime.exec(heightCommand);
        ProcessGetDimensions heightOutput = new ProcessGetDimensions(heightProcess.getInputStream());
        heightOutput.start();
        heightProcess.waitFor();
        videoHeight = Integer.parseInt(heightOutput.returnValue());
        heightProcess.destroy();

        //FPS
        Process fpsProcess = runTime.exec(fpsCommand);
        ProcessGetDimensions fpsOutput = new ProcessGetDimensions(fpsProcess.getInputStream());
        fpsOutput.start();
        fpsProcess.waitFor();
        videoFPS = parseFPS(fpsOutput.returnValue());
        fpsProcess.destroy();

    }

    /**
     * Perform all the tilings specified in the XML file for each video obtained in the previous phase.
     *
     * @throws IOException
     * @throws InterruptedException
     */
    public void tile() throws IOException, InterruptedException {
        Runtime runTime = Runtime.getRuntime();
        Process worker;
        ProcessOutputStream outputStream;
        String commandPrefix = "ffmpeg -y ";
        String commandPortion;
        String commandSuffix;
        String commandOutput;

        int tilingNumber = tilings.size();
        //Checking if tiling is needed
        if (tilingNumber > 0) {
            //Tilings are needed
            tilingVersions = new String[tilingNumber][];
            //For each tiling
            for (int i = 0; i < tilingNumber; i++) {
                Tiling currentTiling = tilings.get(i);
                int tileNumber = currentTiling.gethTiles() * currentTiling.getvTiles();
                tilingVersions[i] = new String[tileNumber * versions.size()];
                System.out.printf("\tTiling the video into %dx%d layout%n", currentTiling.gethTiles(), currentTiling.getvTiles());
                //For each bitrate version
                for (int j = 0; j < versions.size(); j++) {
                    System.out.println("\tTiling the video into CRF=" + versions.get(j).getCRF());
                    //For each tile
                    for (int k = 0; k < tileNumber; k++) {
                        Tile currentTile = currentTiling.getTiles().get(k);
                        System.out.print(currentTile.generateLog() + "...");
                        commandSuffix = "-hide_banner " + currentTile.generateCommandPortion(videoWidth, videoHeight);
                        commandOutput = String.format("%s/%dx%d", output, currentTiling.gethTiles(), currentTiling.getvTiles()) + "/" + outputName + currentTile.generateFileSuffix() + versions.get(j).generateFileSuffix() + ".mp4";
                        commandPortion = "-i " + videoPath + " -loglevel warning " + versions.get(j).generateCommandPortion(videoFPS);
                        worker = runTime.exec(commandPrefix + commandPortion + commandSuffix + commandOutput);
                        outputStream = new ProcessOutputStream(worker.getErrorStream());
                        outputStream.start();
                        worker.waitFor();
                        worker.destroy();
                        //Storing the current tile for the next phase
                        tilingVersions[i][(j * tileNumber) + k] = commandOutput
                                + "#video:desc_as='<SupplementalProperty schemeIdUri=\"urn:mpeg:dash:srd:2014\""
                                + " value=\"0," + currentTile.generateSRDProperty() + "," + currentTiling.gethTiles() + "," + currentTiling.getvTiles() + "\"/>'";

                        System.out.println(" completed!");
                    }
                    System.out.println("\t Work finished on CRF=" + versions.get(j).getCRF());
                }
                System.out.println("\t Work finished on the " + String.format("%dx%d", currentTiling.gethTiles(), currentTiling.getvTiles()) + " layout tiling.");
            }
        } else {
            //No tiling is needed. Just copying the video paths and the audio for the last phase
            tilingVersions = new String[1][versions.size()];
            for (int i = 0; i < versions.size(); i++) {
                tilingVersions[0][i] = videoPath + "#video";
            }
        }
    }

    /**
     * All the versions and all the tiles are grouped to generate segments together with the MPD file.
     *
     * @throws IOException
     * @throws InterruptedException
     */
    public void segment() throws IOException, InterruptedException {
        for (int i = 0; i < tilings.size(); i++) {
            Tiling currentTiling = tilings.get(i);
            for (int segment : segments) {
                String outputDir = String.format("%s/%dx%d/%d", output, currentTiling.gethTiles(), currentTiling.getvTiles(), segment);

                Runtime runTime = Runtime.getRuntime();
                Process worker;
                ProcessOutputStream outputStream;
                StringBuilder command = new StringBuilder("MP4Box -dash " + segment + " -segment-name %s_ -rap -out " + outputDir + "/manifest.mpd ");

                //Putting each tile/version as input for MP4Box
                for (int k = 0; k < tilingVersions[i].length; k++) {
                    command.append(tilingVersions[i][k]).append(" ");
                }

                //Creating a temporary script
                String tempDashFilename = String.format("%s/%dx%d", output, currentTiling.gethTiles(), currentTiling.getvTiles()) + "/dash.sh";
                PrintStream writer = new PrintStream(new FileOutputStream(tempDashFilename));
                writer.println(command);
                writer.close();

                //Adding execution privileges
                worker = runTime.exec("chmod 777 " + tempDashFilename);
                outputStream = new ProcessOutputStream(worker.getErrorStream());
                outputStream.start();
                worker.waitFor();
                worker.destroy();

                //Executing the script
                worker = runTime.exec(tempDashFilename);
                outputStream = new ProcessOutputStream(worker.getErrorStream());
                outputStream.start();
                worker.waitFor();
                worker.destroy();
            }
        }
    }

    private int parseFPS(String fracFPS) {
        fracFPS = fracFPS.substring(1, fracFPS.length() - 1);
        if (fracFPS.contains("/")) {
            String[] ratio = fracFPS.split("/");
            return (int) Math.round(Double.parseDouble(ratio[0]) / Double.parseDouble(ratio[1]));
        } else {
            return (int) Math.round(Double.parseDouble(fracFPS));
        }
    }
}
