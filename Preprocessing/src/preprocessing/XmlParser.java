/*
 * SMART360 Preprocessing
 * Copyright (C) 2023  Quentin Guimard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * This file contains code under the following license:
 *
 *
 * Copyright 2017 Université Nice Sophia Antipolis (member of Université Côte d'Azur), CNRS
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package preprocessing;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;


public class XmlParser {

    private final String filename;

    public XmlParser(String filename) {
        this.filename = filename;
    }

    /**
     * In this function the DOM of the XML file is obtained: if the file has errors
     * in the XML structure an exception is thrown.
     * If the DOM is correctly retrieved the DOM object is passed to the "parseVideo" method
     * in order to perform a second parsing related to the file content.
     *
     * @return list of work to do containing versions and tiles desired
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public Worklist parseXml() throws ParserConfigurationException, SAXException, IOException {
        File xmlFile = new File(filename);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        //Check if the XML is syntactically corrected
        Document doc = dBuilder.parse(xmlFile);
        Element videoElement = doc.getDocumentElement();
        videoElement.normalize();
        //Check if the XML is semantically corrected
        return parseVideo(videoElement);
    }

    /**
     * Checks whether the root name has the proper tag value
     *
     * @param video element to be parsed
     * @throws SAXException
     */
    private void parseRootNodeName(Element video) throws SAXException {
        if (!(video.getNodeName().equals(K.videoTag))) {
            throw new SAXException("The root XML tag must be '" + K.videoTag + "'!");
        }
    }

    /**
     * Checks whether the video path has been specified just once
     *
     * @param video    element to be parsed
     * @param worklist in which the video path attribute will be set
     * @throws SAXException
     */
    private void parseVideoPath(Element video, Worklist worklist) throws SAXException {
        NodeList paths = video.getElementsByTagName(K.videoPathTag);
        int pathLenght = paths.getLength();
        if (pathLenght == 0) {
            //Attribute is mandatory
            throw new SAXException("The video path has not been specified in the '" + K.videoPathTag + "' tag!");
        } else if (pathLenght > 1) {
            //Illegal length
            throw new SAXException("It is not possible to specify more than one path for a single video!");
        } else {
            //Add the path
            worklist.setVideoPath(paths.item(0).getTextContent());
        }
    }

    /**
     * If specified, CRF versions are added to the worklist.
     * A single version leads to a different encoding of the video.
     *
     * @param video    element to be parsed
     * @param worklist in which the versions will be saved
     * @throws SAXException
     */
    private void parseCRFs(Element video, Worklist worklist) throws SAXException {
        NodeList CRFs = video.getElementsByTagName(K.videoCRFsTag);
        int CRFsLength = CRFs.getLength();
        if (CRFsLength == 0) {
            throw new SAXException("Missing CRFs tag.");
        } else if (CRFsLength > 1) {
            throw new SAXException("Too many CRFs tags (more than maximum of 1).");
        } else {
            NodeList CRFList = ((Element) CRFs.item(0)).getElementsByTagName(K.CRFTag);
            int bitrateLength = CRFList.getLength();
            if (bitrateLength < 1) {
                throw new SAXException("You need to specify at least one CRF inside the CRFs element.");
            } else {
                for (int i = 0; i < CRFList.getLength(); i++) {
                    worklist.addVersion(parseCRF((Element) CRFList.item(i)));
                }
            }
        }
    }

    /**
     * Creates a new version from the specified CRF
     *
     * @param CRF to be parsed
     * @return version with all the attribute set
     */
    private Version parseCRF(Element CRF) {
        return new Version(Integer.parseInt(CRF.getTextContent()));
    }

    /**
     * If specified, tilings are added to the worklist.
     *
     * @param video    element to be parsed
     * @param worklist in which the versions will be saved
     * @throws SAXException
     */
    private void parseTilings(Element video, Worklist worklist) throws SAXException {
        NodeList tilings = video.getElementsByTagName(K.videoTilingsTag);
        int tilingsLength = tilings.getLength();
        if (tilingsLength == 0) {
            throw new SAXException("Missing tilings tag.");
        } else if (tilingsLength > 1) {
            throw new SAXException("Too many tilings tags (more than maximum of 1).");
        } else {
            NodeList tilingList = ((Element) tilings.item(0)).getElementsByTagName(K.tilingTag);
            int tilingLength = tilingList.getLength();
            if (tilingLength < 1) {
                throw new SAXException("You need to specify at least one tiling inside the tilings element.");
            } else {
                for (int i = 0; i < tilingList.getLength(); i++) {
                    worklist.addTiling(parseTiling((Element) tilingList.item(i)));
                }
            }
        }
    }

    /**
     * Parses a tiling element checking that the format is WxH
     *
     * @param tiling element to be parsed
     * @throws SAXException
     */
    private Tiling parseTiling(Element tiling) throws SAXException {
        try {
            String[] tilingStrings = tiling.getTextContent().split("x");
            int hTiles = Integer.parseInt(tilingStrings[0]);
            int vTiles = Integer.parseInt(tilingStrings[1]);
            return new Tiling(hTiles, vTiles);
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new SAXException("Tiling must be described as WxH string.");
        }
    }

    /**
     * If specified, segments are added to the worklist.
     *
     * @param video    element to be parsed
     * @param worklist in which the versions will be saved
     * @throws SAXException
     */
    private void parseSegments(Element video, Worklist worklist) throws SAXException {
        NodeList segments = video.getElementsByTagName(K.videoSegmentsTag);
        int segmentsLength = segments.getLength();
        if (segmentsLength == 0) {
            throw new SAXException("Missing segments tag.");
        } else if (segmentsLength > 1) {
            throw new SAXException("Too many segments tags (more than maximum of 1).");
        } else {
            NodeList segmentList = ((Element) segments.item(0)).getElementsByTagName(K.segmentTag);
            int segmentLength = segmentList.getLength();
            if (segmentLength < 1) {
                throw new SAXException("You need to specify at least one segment inside the segments element.");
            } else {
                for (int i = 0; i < segmentList.getLength(); i++) {
                    worklist.addSegment(parseSegment((Element) segmentList.item(i)));
                }
            }
        }
    }

    /**
     * Parses segment duration integer
     *
     * @param segment element to be parsed
     */
    private int parseSegment(Element segment) {
        return Integer.parseInt(segment.getTextContent());
    }

    /**
     * Checks whether the output folder has been specified just once
     *
     * @param video    element to be parsed
     * @param worklist in which the output-folder attribute will be set
     * @throws SAXException
     */
    private void parseOutput(Element video, Worklist worklist) throws SAXException {
        NodeList outputs = video.getElementsByTagName(K.outputTag);
        int outputLenght = outputs.getLength();
        if (outputLenght == 0) {
            //Attribute is mandatory
            throw new SAXException("The output folder has not been specified in the '" + K.outputTag + "' tag!");
        } else if (outputLenght > 1) {
            //Illegal length
            throw new SAXException("It is not possible to specify more than one output folder for a single video!");
        } else {
            //Add the output folder
            worklist.setOutput(outputs.item(0).getTextContent());
        }
    }

    /**
     * In this method the content of the XML file is checked in order to ensure
     * that all the mandatory parameters are present and all of them are correctly inserted.
     *
     * @param video element to be parsed
     * @return list of work to do containing versions and tiles desired
     * @throws SAXException
     */
    private Worklist parseVideo(Element video) throws SAXException {
        Worklist worklist = new Worklist();

        //STEP 1 - Checking if the root node has the right tag
        parseRootNodeName(video);

        //STEP 2 - Checking if the video path is defined
        parseVideoPath(video, worklist);

        //STEP 3 - Checking if the bitrates are defined
        parseCRFs(video, worklist);

        //STEP 4 - Checking if the tilings are defined
        parseTilings(video, worklist);

        //STEP 5 - Checking if the segments are defined
        parseSegments(video, worklist);

        //STEP 6 - Checking if the output folder is defined
        parseOutput(video, worklist);

        return worklist;
    }
}
