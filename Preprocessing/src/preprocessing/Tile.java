/*
 * SMART360 Preprocessing
 * Copyright (C) 2023  Quentin Guimard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * This file contains code under the following license:
 *
 *
 * Copyright 2017 Université Nice Sophia Antipolis (member of Université Côte d'Azur), CNRS
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package preprocessing;

public class Tile {

    private final int x; //x position in the grid
    private final int y; //y position in the grid
    private final int w; //grid weight
    private final int h; //grid height
    private final float startXPerc; //x-Percentage of the original video where the tiling must begin
    private final float startYPerc; //y-Percentage of the original video where the tiling must begin
    private final float endXPerc; //x-Percentage of the original video where the tiling must end
    private final float endYPerc; //y-Percentage of the original video where the tiling must end

    public Tile(int x, int y, int w, int h, float startXPerc, float startYPerc, float endXPerc, float endYPerc) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
        this.startXPerc = startXPerc;
        this.startYPerc = startYPerc;
        this.endXPerc = endXPerc;
        this.endYPerc = endYPerc;
    }

    /**
     * Generates the crop filter description according to which the tiling must be performed
     *
     * @param width  of the video to be tiled
     * @param height of the video to be tiled
     * @return String containing the crop filter description to properly tile the video
     */
    public String generateCommandPortion(int width, int height) {
        int startWpixel = (int) (width * startXPerc);
        int startHpixel = (int) (height * startYPerc);
        int endWpixel = (int) (width * endXPerc);
        int endHpixel = (int) (height * endYPerc);
        int areaWidth = endWpixel - startWpixel;
        int areaHeight = endHpixel - startHpixel;
        return " -filter:v crop=" + areaWidth + ":" + areaHeight + ":" + startWpixel + ":" + startHpixel + " ";
    }

    /**
     * Creates a log message according to the tile position and size
     *
     * @return String with the message related to the tile position and size
     */
    public String generateLog() {
        return "\t\tGenerating the tile with position (" + x + "," + y + ") and size (" + w + "," + h + ")";
    }

    /**
     * Creates a suffix to be added to the file according to the tile position and size
     *
     * @return String with the suffix
     */
    public String generateFileSuffix() {
        return "-" + x + "" + y + "" + w + "" + h;
    }

    /**
     * Generates the SRD property of the tile according to the format x,y,w,h
     *
     * @return String with the SRD property
     */
    public String generateSRDProperty() {
        return x + "," + y + "," + w + "," + h;
    }
}
