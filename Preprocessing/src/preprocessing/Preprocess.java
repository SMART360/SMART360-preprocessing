/*
 * SMART360 Preprocessing
 * Copyright (C) 2023  Quentin Guimard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * This file contains code under the following license:
 *
 *
 * Copyright 2017 Université Nice Sophia Antipolis (member of Université Côte d'Azur), CNRS
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package preprocessing;

import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

public class Preprocess {
    public static void main(String[] args) {

        for (String arg : args) {
            Worklist worklist;

            try {
                //Building the parser object
                XmlParser xmlParser = new XmlParser(arg);

                //Starting process
                System.out.println("Process Started with file " + arg + "...");

                //Parsing the file
                System.out.print("Parsing the XML file...");
                worklist = xmlParser.parseXml();
                System.out.println(" completed!");

                //Preliminary actions
                System.out.print("Preliminary actions...");

                //Checking if the video file exists
                worklist.videoExists();

                //Checking if the output folder exists
                worklist.outputDirectory();

                //Acquiring video dimensions
                worklist.videoDimensions();
                System.out.println(" completed!");

                //PHASE 1 - Transcoding cropped videos
                System.out.println("Start tiling:");
                worklist.tile();
                System.out.println("Tiling complete!");

                //PHASE 2 - Segmenting videos
                System.out.println("Start Segmenting:");
                worklist.segment();
                System.out.println("Segmenting complete!");

                System.out.println("Process completed with file " + arg + "!");

            } catch (ParserConfigurationException | SAXException | InterruptedException | IOException e) {
                System.err.println(e.getMessage());
            }
        }

    }
}
