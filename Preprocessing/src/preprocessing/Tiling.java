/*
 * SMART360 Preprocessing
 * Copyright (C) 2023  Quentin Guimard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * This file contains code under the following license:
 *
 *
 * Copyright 2017 Université Nice Sophia Antipolis (member of Université Côte d'Azur), CNRS
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package preprocessing;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

public class Tiling {

    private final int hTiles;
    private final int vTiles;
    private final List<Tile> tiles;

    public Tiling(int hTiles, int vTiles) {
        this.hTiles = hTiles;
        this.vTiles = vTiles;
        this.tiles = new ArrayList<>();

        for (int i = 0; i < hTiles; i++) {
            for (int j = 0; j < vTiles; j++) {
                float startXPerc = BigDecimal.valueOf(i / (double) hTiles).setScale(2, RoundingMode.HALF_EVEN).floatValue();
                float startYPerc = BigDecimal.valueOf(j / (double) vTiles).setScale(2, RoundingMode.HALF_EVEN).floatValue();
                float endXPerc = BigDecimal.valueOf((i + 1) / (double) hTiles).setScale(2, RoundingMode.HALF_EVEN).floatValue();
                float endYPerc = BigDecimal.valueOf((j + 1) / (double) vTiles).setScale(2, RoundingMode.HALF_EVEN).floatValue();
                tiles.add(new Tile(i, j, 1, 1, startXPerc, startYPerc, endXPerc, endYPerc));
            }
        }
    }

    public int gethTiles() {
        return hTiles;
    }

    public int getvTiles() {
        return vTiles;
    }

    public List<Tile> getTiles() {
        return tiles;
    }
}
