#
# SMART360 Preprocessing
# Copyright (C) 2023  Quentin Guimard
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import os
import xml.etree.ElementTree as ET

parameters = {
    'input_videos': tuple([f'./source/videos/{f[:-1]}' for f in open('./source/videos/test_videos', 'r')]),
    'CRFs': (16, 22, 28, 34, 40),
    'tile_layouts': ('12x6', ),
    'segment_lengths': (1, )
}

if not os.path.exists('./xml/'):
    os.makedirs('./xml/')

for input_video in parameters['input_videos']:
    video = ET.Element('video')

    path = ET.SubElement(video, 'path')
    path.text = input_video

    CRFs = ET.SubElement(video, 'CRFs')

    for CRF in parameters['CRFs']:
        bitrate = ET.SubElement(CRFs, 'CRF')
        bitrate.text = str(CRF)

    tilings = ET.SubElement(video, 'tilings')

    for tile_layout in parameters['tile_layouts']:
        tiling = ET.SubElement(tilings, 'tiling')
        tiling.text = tile_layout

    segments = ET.SubElement(video, 'segments')

    for segment_length in parameters['segment_lengths']:
        segment = ET.SubElement(segments, 'segment')
        segment.text = str(segment_length * 1000)

    output = ET.SubElement(video, 'output')
    output.text = f'./output/{input_video[16:-4]}'

    tree = ET.ElementTree(video)
    tree.write(f'./xml/{input_video[16:-4]}.xml', encoding='utf-8', xml_declaration=True)
