#
# SMART360 Preprocessing
# Copyright (C) 2023  Quentin Guimard
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import math
import json
import os
from pathlib import Path
import subprocess

import numpy as np
from tqdm.auto import tqdm

SOURCE_VIDEO_DIR = './source/videos'
OUTPUT_ROOT_DIR = './output'
VIDEO_JSON_DST_DIR = './json_output/video_manifests'
N_QUALITIES = 5


def tile_xy_to_id(x, y, w):
    return x + y * w


for video in tqdm(sorted(os.listdir(OUTPUT_ROOT_DIR))):
    if video == 'old':
        continue
    video_duration = float(subprocess.run(['ffprobe', '-i', f'{SOURCE_VIDEO_DIR}/{video}.mp4', '-show_entries',
                                           'format=duration', '-v', 'quiet', '-of', 'csv=p=0'], stdout=subprocess.PIPE)
                           .stdout.decode()[:-1])
    Path(f'{VIDEO_JSON_DST_DIR}/{video}/').mkdir(parents=True, exist_ok=True)
    video_path = os.path.join(OUTPUT_ROOT_DIR, video)
    for tiling in os.listdir(video_path):
        w, h = map(int, tiling.split('x'))
        tiling_path = os.path.join(video_path, tiling)
        for segment in os.listdir(tiling_path):
            try:
                segment = int(segment)
            except ValueError:
                continue
            n_segments = math.ceil(video_duration * 1000 / segment)
            segment_path = os.path.join(tiling_path, str(segment))
            json_video_file = f'{VIDEO_JSON_DST_DIR}/{video}/{tiling}_{segment}.json'
            json_content = {
                'segment_duration_ms': segment,
                'tiles_x': w,
                'tiles_y': h,
                'bitrates_kbps': [],
                '_comment_': 'segment_sizes_bits[segment_index][tile_index][bitrate_index]',
                'segment_sizes_bits': []
            }
            segment_sizes_bits = [[[] for _ in range(w * h)] for _ in range(n_segments)]
            for f in os.listdir(segment_path):
                if f[-4:] != '.m4s':
                    continue
                segment_number = int(f[:-4].split('_')[-1]) - 1
                xy = f.split('-')[1][:-2]
                x = int(xy[:-1])
                y = int(xy[-1])
                segment_sizes_bits[segment_number][tile_xy_to_id(x, y, w)].append(os.path.getsize(f'{segment_path}/{f}') * 8)
            to_del = []
            for i, seg in enumerate(segment_sizes_bits):
                if [item for sublist in seg for item in sublist]:
                    for j, tile in enumerate(seg):
                        if tile:
                            segment_sizes_bits[i][j] = sorted(tile)
                        else:
                            segment_sizes_bits[i][j] = [0 for _ in range(N_QUALITIES)]
                else:
                    to_del.append(i)
            for i in reversed(to_del):
                del segment_sizes_bits[i]
            json_content['bitrates_kbps'] = (np.array(segment_sizes_bits).sum(0).sum(0) / (video_duration * 1000)).round().astype(int).tolist()
            json_content['segment_sizes_bits'] = segment_sizes_bits
            json.dump(json_content, open(json_video_file, 'w'))
